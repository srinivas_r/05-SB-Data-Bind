
<%@taglib uri = "http://www.springframework.org/tags/form"  prefix = "form" %>

<%@taglib uri = "http://java.sun.com/jsp/jstl/core"  prefix = "c" %>

<%@page isELIgnored = "false" %>

<c:if test = "${message ne null}">
   <font color = "red"> <c:out  value = "${message}" /> </font>
</c:if>
<br>
<form:form  action = "check"  method = "post"  modelAttribute = "credentials">
     <table>
       <tr>
        <td>
		   Username : 
		</td>
		<td>
		  <form:input  path = "username" />  
		</td>
		<td>
		  <font color='red'> <form:errors path = "username" /> </font>
		</td>
	   </tr>
	   <tr>
	     <td>	  
		   Password : 
		 </td>
		 <td>
		   <form:password path="password" />   
		 </td>
		 <td>
		  <font color='red'> <form:errors path = "password" /> </font>
		</td>
	   </tr>
	 </table>  	 
		<input type = "submit"  value = "submit">
</form:form>
